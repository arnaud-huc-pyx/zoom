/**
 *  Zoom
 *
 *  a jQuery/Zepto plugin for zoom on product, must be use with Swiper http://www.idangero.us/
 *  only work tap/doubleTap Event
 *
 *	You can style .loader (who appeared when double TAP)
 *
 *
 */
(function($) {
	var clickEvent = 'tap click',
			dblClickEvent = 'doubleTap dblclick',
			dragOnEvent = 'touchstart dragstart',
			dragEndEvent = 'touchend dragend',
			dragEvent = 'touchmove drag'

	$.fn.zoom = function() {


		var loader = '<div id="loader" class="loader-layer"><div class="loader spin"></div></div>',
			info = '<div id="info" class="copy-alpha fadeOut" style="position:absolute;top:40%;left:50%;margin-left:-86px;width:173px;padding:14px 18px;background-color:rgba(255, 255, 255, .85);box-shadow:-1px 2px 6px rgba(0, 0, 0, .6), 1px 0 6px rgba(0, 0, 0, .6);pointer-events: none;">Double tap pour zoomer</div>',
			frames = $('.swiper-container').length ? $('.swiper-container') : $('.zoomFrame'),
			images = frames.find('img'),
			frameGeo = frames[0].getBoundingClientRect(),
			touchstartX, touchstartY,
			deltaX = 0,
			deltaY = 0,
			newDeltaX = 0,
			newDeltaY = 0,
			deltaMaxX, deltaMaxY;
		frames[0].style.position = 'relative';
		frames[0].style.overflow = 'hidden';

		frames.length > 1 ? frames.eq(1).append(info) : frames.eq(0).append(info);
		var info = document.getElementById('info');

		$(info).one('webkitAnimationEnd animationend', function() {
			this.parentNode.removeChild(this);
			info = '';
		})

		var handleTap = function() {
			touchstartX = event.touches[0].pageX;
			touchstartY = event.touches[0].pageY;
		}

		var handleDoubleTap = function(e) {
			// prevent touch while scrolling
			event.preventDefault();
			this.classList.contains('zoomed') ? handleZoomOut(this) : handleZoomIn(this);
		}

		var handleZoomIn = function(image) {
			// Remove swiper event
			if ($('.swiper-container').length > 0) {
				slider.mySwiperFa.params.onlyExternal = true;
			}
			$(image).off(dragOnEvent, handleTap);
			$(image).off(dblClickEvent, handleDoubleTap);
			frames.append(loader);

			var zoomIn = function() {
				image.style.webkitTransition = 'all .3s linear';
				image.style.MozTransition = 'all .3s linear';

				image.style.webkitTransform = 'scale3d(2.5, 2.5, 1) translate3d(0, 0, 0)';
				image.style.MozTransform = 'scale3d(2.5, 2.5, 1) translate3d(0, 0, 0)';

				$(image).on('webkitTransitionEnd transitionend', function() {
					image.classList.add('zoomed');

					//Css animation on doubleTap for div.loader
					var loader = document.getElementById('loader');
					loader.style.webkitTransition = 'opacity .15s linear';
					loader.style.MozTransition = 'opacity .15s linear';
					loader.style.opacity = '0';
					$(loader).one('webkitTransitionEnd transitionend', function() {
						this.parentNode.removeChild(this)
					});

					image.style.webkitTransition = 'none';
					image.style.MozTransition = 'none';
					var imageGeo = image.getBoundingClientRect();
					deltaMaxX = (frameGeo.left - imageGeo.left) / 2.5;
					deltaMaxY = (frameGeo.top - (imageGeo.top + document.documentElement.scrollTop)) / 2.5;

					$(image).on(dragOnEvent, handleTap);
					$(image).on(dblClickEvent, handleDoubleTap);
					$(image).on(dragEvent, handleMove);
					$(image).on(dragEndEvent, handleEnd);
				})
			}
			// Check if image has been zoomed in the past
			if (image.hasAttribute('data-zoomed')) {
				zoomIn();
			} else {
				image.dataset.zoomed = '';
				if (document.getElementById('info')) {
					info.parentNode.removeChild(info);
				};
				image.src = image.dataset.bigImage+'?42';//Response to bug
				image.addEventListener('load', zoomIn, false);
			}
		}


		/**
 			*  @param {object} image - Current image
 			*
 			*/

		var handleZoomOut = function(image) {
			$(image).off(dragOnEvent, handleTap);
			$(image).off(dblClickEvent, handleDoubleTap);

			image.style.webkitTransition = 'all .3s linear';
			image.style.MozTransition = 'all .3s linear';

			image.style.webkitTransform = 'scale3d(1, 1, 1) translate3d(0, 0, 0)';
			image.style.MozTransform = 'scale3d(1, 1, 1) translate3d(0, 0, 0)';

			$(image).on('webkitTransitionEnd transitionend', function() {
				image.classList.remove('zoomed');

				image.style.webkitTransition = 'false';
				image.style.MozTransition = 'false';

				// reset delta
				newDeltaX = 0;
				newDeltaY = 0;

				// re-init swiper event
				if ($('.swiper-container').length > 0) {
					slider.mySwiperFa.params.onlyExternal = false
				};
				$(image).on(dragOnEvent, handleTap);
				$(image).on(dblClickEvent, handleDoubleTap);
				$(image).off(dragEvent, handleMove);
				$(image).off(dragEndEvent, handleEnd);
			}, false);
		}

		var handleMove = function() {
			// prevent scrolling while touchmove
			event.preventDefault();

			deltaX = ((event.touches[0].pageX - touchstartX) / 2.5) + newDeltaX;
			deltaY = ((event.touches[0].pageY - touchstartY) / 2.5) + newDeltaY;

			if (Math.abs(deltaX) >= deltaMaxX) {
				deltaX = deltaX >= 0 ? deltaMaxX : -deltaMaxX
			}

			if (Math.abs(deltaY) >= deltaMaxY) {
				deltaY = deltaY >= 0 ? deltaMaxY : -deltaMaxY
			}

			if (Math.abs(deltaX) <= deltaMaxX && Math.abs(deltaY) <= deltaMaxY) {
				this.style.webkitTransform = 'scale3d(2.5, 2.5, 1) translate3d(' + deltaX + 'px, ' + deltaY + 'px, 0)';
				this.style.MozTransform = 'scale3d(2.5, 2.5, 1) translate3d(' + deltaX + 'px, ' + deltaY + 'px, 0)';
			}
		}

		var handleEnd = function() {
			newDeltaX = deltaX;
			newDeltaY = deltaY;
		}

		// when next/prev slider control are clicked
		// check if there is a zoomed image
		// if yes, zoomed it out
		$('body').on(clickEvent, function() {
			if ($('.swiper-slide').find('img.zoomed').length > 0) {
				handleZoomOut($('.swiper-slide').find('img.zoomed')[0]);
			}
		});

		/*****************************************************************************
			* Init Slider for demo
			*
			****************************************************************************/
		var slider = {};
		slider.mySwiperFa = $('.swiper-container').swiper({
			mode:"horizontal",
			pagination:".slider-nav",
			paginationClickable:false,
			speed:300
		});

		$('.carousel-product-image .carousel-previous').on('click', function(e) {
			e.preventDefault();
			slider.mySwiperFa.swipePrev();
		});
		$('.carousel-product-image .carousel-next').on('click', function(e) {
			e.preventDefault();
			slider.mySwiperFa.swipeNext();
		});
		/*****************************************************************************
			* End Init Slider
			*
			****************************************************************************/

		images.on(dragOnEvent, handleTap);
		images.on(dblClickEvent, handleDoubleTap);
	}
})(window.Zepto || window.jQuery);